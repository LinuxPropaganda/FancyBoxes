# FancyBoxes

A python module to enhance terminal/text based interfaces with window-like borders and formatting.

## Installation
Clone the repo and run `sudo ./install-module` in the main folder.
This will make the module available system wide for python3

## Usage
Better docs coming (hopefully) soon, check example for practical usage, it's not that difficult to grasp I promise.

### Hello, World!
Let's start by importing the module and create a box.
```
from fancyboxes import *

mything = Box(title="My first box", width=30)
```
If you run the program now, it will do nothing. That's because we need to display the box!
```
mything.display()
```
Now we see something! Let's change the content of our little thingy.
```
mything.content = "Hello, World!"
```
(This line obviously goes BEFORE we display() the box)

And that's it! Here's the whole code:
```
from fancyboxes import *

mything = Box(title="My first box", width=30)
mything.content = "Hello, World!"

mything.display()
```

You can use offset=number as a kwarg in display to distance it from the left border of the screen. For vertical offsetting, just print a bunch of \n characters

### Displaying multiple boxes
To stack boxes vertically, just display them in order, top to bottom:
```
box1.display()
box2.display()
```
To stack 'em horizontally, we'll need a special function, `display_in_row`:
```
display_in_row(box1, box2) # from right to left
```
offsetting rules apply here as well.

### Change content mk2

If you like compact code, boxes have an `update_content` function that will also return the box object, making things like this possible:
```
Box(title="ID card", width=30).update_content("name: pop\nsurname: bob\nprofession: griefer").display()
```

If you're dealing with logging or scrolling text, `print_in` is the way to go:
```
mybox.print_in("hello") # content="hello"
mybox.print_in("hello", "world") # content="hello\nhello world"
```
This funtion is the only that respects the height parameter of a box (`Box(title="", width=43, height=12)`);
if print_in would break out of the lines limit, it will remove the uppest line to make space (see [example #1](https://codeberg.org/LinuxPropaganda/FancyBoxes/src/branch/main/examples/example.py) )


### There's probably more, but for now that's it :)
Oh btw there's full ansii colour support, idk if it works with colorama or stuff like that, lemme know.




## Contributing
Feel free to pull request any documentation/example/code improvement to help me out!






