from fancyboxes import *
from os import system
from time import sleep
from datetime import datetime as dt


SLEEPTIME = 0.2

header = Box(title="", width=44).update_content("  -= Xtreme FizzBuzzer =-")
footer = Box(title="", width=44).update_content(" running... ")

counter = Box(title="fizzbuzz", width=10, height=10).update_content("\n"*8)
log = Box(title="logger", width=30).update_content("Ready\n")

def refresh():
	print("\n"*5)
	header.display(offset=10)
	display_in_row(counter, log, offset=10)
	footer.display(offset=10)


def fizzbuzz(i):

	out = ""
	if not i%3:
		out += "fizz"
	if not i%5:
		out += "buzz"
	if i%3 and i%5:
		out += str(i)

	return out


def main():
	start = dt.now()
	for i in range(100):
		counter.print_in(fizzbuzz(i))
		if not i%10:
			log.print_in(i, '%', ' completed', joiner='')

		system("clear")
		refresh()
		sleep(SLEEPTIME)

	end = dt.now()
	timepassed = (end-start).total_seconds()

	log.print_in("100% completed")
	footer.content = f"Finished in {timepassed}, of which {timepassed - SLEEPTIME*100} doing operations other than sleeping"


if __name__ == '__main__':
	try: main()
	except KeyboardInterrupt: log.print_in("Operation interrupted by user")
	system("clear"); refresh()