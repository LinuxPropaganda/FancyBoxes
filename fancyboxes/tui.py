'''
Copyright (c) 2022 Lorenzo Mari
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

https://www.gnu.org/licenses/gpl-3.0.txt (or later)
'''

# utils to ease the creation of Terminal User Interfaces

VERSION = "0.1"

TEXTURE =  {
	"top": {          # also cool       ascii
		"left": "┌",  #    ╔              +
		"right": "┐", #    ╗              +
	},
	"bottom": {
		"left": "└",  #    ╚              +
		"right": "┘", #    ╝              +
	},
	"hline": "─",     #    ═              -
	"vline": "│",     #    ║              |
}




class Box:
	def __init__(s, width=20, height=10, title="New Box"):
		s.height = height
		s.width = width+1
		s.title = title

		s.content = "\tI'm empty\n\t  :(\nfill me by changing my .content attribute!"
		s.warp = 1


	def get_lines(s):
		return GetWarpedLines(s.content, s.width)*s.warp + s.content.split('\n')*(not s.warp)


	def get_display_lines(s):
		out = []

		top_side = s.width-len(s.title)-2

		out.append(TEXTURE["top"]["left"]+ TEXTURE["hline"]*2 + s.title + TEXTURE["hline"]*(top_side) + TEXTURE["top"]["right"])
		
		toprint = s.get_lines()

		for j in toprint:
			out.append(TEXTURE["vline"] + j + ' '*(s.width-nocolor_len(j)) + TEXTURE["vline"])

		out.append(TEXTURE["bottom"]["left"]+ TEXTURE["hline"]*(s.width) + TEXTURE["bottom"]["right"])

		return out


	def display(s, offset=0):
		
		to_display = s.get_display_lines()
		for i in to_display:
			print(' '*offset + i)


	def update_content(s, content):
		s.content = content
		return s


	def print_in(s, *txt, end='\n', joiner=' '):

		s.content += joiner.join([str(i) for i in txt])
		s.content+=end
		
		lines = GetWarpedLines(s.content, s.width)
		if len(lines) > s.height:
			s.content = '\n'.join(lines[len(lines)-s.height:])




def GetWarpedLines(content, width):
	out = []
	for i in content.split('\n'):
		toprint = [i]

		fixed = 0
		while not fixed:
			fixed = 1

			j = toprint[-1].replace('\t', '       ')
			linelen = nocolor_len(j)

			if linelen >= width:
				toprint = toprint[:-1] + [j[:width-1], j[width-1:]]
				fixed = 0

		for j in toprint:
			out.append(j)

	return out



def nocolor_len(line:str):
	linelen = 0

	counting = 1
	for i in line:
		if i == '\33': counting = 0
		linelen += counting
		if i=='m' and not counting: counting = 1

	return linelen



def display_in_row(*boxes, gap=1, offset=0):

	print(' '*offset, end='')
	for s in boxes:
		print(TEXTURE["top"]["left"]+ TEXTURE["hline"]*2 + s.title + TEXTURE["hline"]*(s.width-len(s.title)-2) + TEXTURE["top"]["right"], end=' '*gap)
	print()

	lines = {}

	height = 0
	for s in boxes:
		n_height = len(s.get_lines())
		if n_height > height:
			height = n_height

	for s in boxes:
		to_line = s.get_lines()
		for i in range(len(to_line)):
			if i not in lines:
				lines[i] = ""

			lines[i] += TEXTURE["vline"] + to_line[i] + ' '*(s.width-nocolor_len(to_line[i])) + TEXTURE["vline"] +' '*gap

		for j in range(i+1, height):
			if j not in lines:
				lines[j] = ""
			lines[j] += TEXTURE["vline"] + ' '*s.width + TEXTURE["vline"] + ' '*gap


	#print(lines)

	for line in lines:
		print(' '*offset+lines[line])


	print(' '*offset, end='')
	for s in boxes:
		print(TEXTURE["bottom"]["left"]+ TEXTURE["hline"]*(s.width) + TEXTURE["bottom"]["right"], end=" "*gap)
	print()






# example usage

if __name__ == "__main__":

	from getch import getch # if you don't have getch, don't mind it's just for the demo, comment out me and use input()
	from random import choice
	import os

	print("\n\n")

	example = Box(title="Example", width=44)
	another = Box(title="")
	
	example.content = "Hello, World! How you doin'?\n\nCheck deez boxes\n\ngibbrish gibbrish gibbrish"
	another.content = "So cool! So sexy!"

	yet_a_new_one = Box(title="").update_content("Faster unreadable way! (Also text wraps, innit cool?)")

	tooltip = Box(title="", width=44).update_content("Q: quit    Any other key: increase entropy")

	example.display()
	display_in_row(another, yet_a_new_one)
	tooltip.display()

	print("\n\n")

	cmd = getch()
	while cmd.lower() != "q":

		print("\n"*30) # kinda cringe
		#os.system("clear") # kinda slow
		# just choose one

		example.content = ''.join([chr(ord(i)+choice([0]*30 + [1, -1])) for i in example.content])
		example.display()
		tooltip.display()

		print("\n"*24)

		cmd = getch()
		
		if cmd == "E":
	
			example.content